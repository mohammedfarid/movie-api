package com.farid.moviesapi.ui;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.farid.moviesapi.App;
import com.farid.moviesapi.R;
import com.farid.moviesapi.interfaceFragment.FragmentCommunicator;
import com.farid.moviesapi.model.Result;
import com.squareup.picasso.Picasso;

public class OverViewFragment extends Fragment implements FragmentCommunicator {

    private ImageView mMoviePosterIv;
    private TextView mMovieTitle;
    private TextView mMovieRate;
    private TextView mMovieOverview;
    private Result mMovieResult;
    public Context context;


    public OverViewFragment() {
        // Required empty public constructor
        if (getArguments() != null) {

        }
    }

    public static OverViewFragment newInstance() {
        return new OverViewFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
        ((MovieActivity) context).fragmentCommunicator = this;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_overview, container, false);
        mMoviePosterIv = view.findViewById(R.id.movie_poster_iv);
        mMovieTitle = view.findViewById(R.id.movie_title_tv);
        mMovieRate = view.findViewById(R.id.movie_tv_star);
        mMovieOverview = view.findViewById(R.id.movie_overview_tv);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ((MovieActivity) context).setUIListeners();
    }

    @Override
    public void passDataToFragment(Result movieResult) {
        mMovieResult = movieResult;
        mMovieTitle.setText(mMovieResult.getOriginalTitle());
        mMovieOverview.setText(mMovieResult.getOverview());
        mMovieRate.setText(mMovieResult.getVoteAverage() + "");
        Picasso.get()
                .load(App.API_IMAGE_URL_V2 + mMovieResult.getPosterPath())
                .into(mMoviePosterIv);
    }

}
