package com.farid.moviesapi.ui;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.farid.moviesapi.R;
import com.farid.moviesapi.adapter.ReviewsRvAdapter;
import com.farid.moviesapi.interfaceFragment.FragmentReviewCommunicator;
import com.farid.moviesapi.model.ResultReviews;

import java.util.List;

public class ReviewFragment extends Fragment implements FragmentReviewCommunicator {

    private RecyclerView mReviewRv;
    private TextView mReviewTv;
    private List<ResultReviews> mResultReviews;
    private ReviewsRvAdapter mReviewsRvAdapter;
    public Context context;

    public ReviewFragment() {
        // Required empty public constructor
        if (getArguments() != null) {

        }
    }

    public static ReviewFragment newInstance() {
        return new ReviewFragment();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
        ((MovieActivity) context).fragmentReviewCommunicator = this;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_reviews, container, false);

        mReviewRv = view.findViewById(R.id.mMoviesReviewsRv);
        mReviewTv = view.findViewById(R.id.mMoviesReviewsTv);
        mReviewRv.setLayoutManager(new LinearLayoutManager(getActivity()));

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }

    @Override
    public void passToFragment(List<ResultReviews> resultReviews) {
        mResultReviews = resultReviews;
        if (mResultReviews.isEmpty()) {
            mReviewTv.setVisibility(View.VISIBLE);
            mReviewRv.setVisibility(View.GONE);
        } else {
            mReviewTv.setVisibility(View.GONE);
            mReviewRv.setVisibility(View.VISIBLE);
            mReviewsRvAdapter = new ReviewsRvAdapter(getActivity(), mResultReviews);
            mReviewRv.setAdapter(mReviewsRvAdapter);
        }
    }
}
