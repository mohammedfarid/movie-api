package com.farid.moviesapi.ui;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.farid.moviesapi.App;
import com.farid.moviesapi.R;
import com.farid.moviesapi.apiHandler.BaseApi;
import com.farid.moviesapi.apiHandler.BaseApiHandler;
import com.farid.moviesapi.interfaceFragment.FragmentCommunicator;
import com.farid.moviesapi.interfaceFragment.FragmentReviewCommunicator;
import com.farid.moviesapi.model.Result;
import com.farid.moviesapi.model.ResultReviews;
import com.farid.moviesapi.model.Reviews;
import com.farid.moviesapi.service.Utils;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MovieActivity extends AppCompatActivity {

    private Result mMovieResult;
    private List<ResultReviews> mResultReviews;
    private Reviews mReviews;
    private ViewPager viewPager;
    private ViewPagerAdapter adapter;
    private TabLayout tabLayout;

    public FragmentCommunicator fragmentCommunicator;
    public FragmentReviewCommunicator fragmentReviewCommunicator;

    private OverViewFragment mOverViewFragment;
    private ReviewFragment mReviewFragment;

    private BaseApi mServiceApi;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movie);
        mMovieResult = getIntent().getParcelableExtra("MovieResult");
        mServiceApi = BaseApiHandler.setupBaseApi().create(BaseApi.class);

        viewPager = findViewById(R.id.pager);
        adapter = new ViewPagerAdapter(getSupportFragmentManager());
        // Add Fragments to adapter one by one
        mOverViewFragment = OverViewFragment.newInstance();
        mReviewFragment = ReviewFragment.newInstance();
        adapter.addFragment(mOverViewFragment, "OverView");
        adapter.addFragment(mReviewFragment, "Reviews");
        viewPager.setAdapter(adapter);

        tabLayout = findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
        if (Utils.isConnectionOn(this)) {
            Call<Reviews> call = mServiceApi.
                    getMoviesReviews(mMovieResult.getId() + "", App.API_KEY);
            call.enqueue(new Callback<Reviews>() {
                @Override
                public void onResponse(Call<Reviews> call, Response<Reviews> response) {
                    if (response.isSuccessful()) {
                        try {
                            mReviews = response.body();
                            if (mReviews != null) {
                                mResultReviews = mReviews.getResults();
                                setUIListenersReviews();
                            }

                        } catch (Exception e) {
                            Log.e("Movie List Ex ", e.getMessage() + " ");
                        }

                    } else {
                        Log.e("Movie List Ex ", "else inside ");
                    }
                }

                @Override
                public void onFailure(Call<Reviews> call, Throwable t) {

                }
            });


        } else {
            Log.e("Movie List Ex ", "else");
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    public void setUIListeners() {
        if (fragmentCommunicator != null)
            fragmentCommunicator.passDataToFragment(mMovieResult);
    }

    public void setUIListenersReviews() {
        if (fragmentReviewCommunicator != null)
            fragmentReviewCommunicator.passToFragment(mResultReviews);
    }

    // Adapter for the viewpager using FragmentPagerAdapter
    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }


}
