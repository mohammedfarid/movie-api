package com.farid.moviesapi.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;

import com.farid.moviesapi.App;
import com.farid.moviesapi.R;
import com.farid.moviesapi.adapter.MoviesRvAdapter;
import com.farid.moviesapi.apiHandler.BaseApi;
import com.farid.moviesapi.apiHandler.BaseApiHandler;
import com.farid.moviesapi.interfaceRecycleView.RecyclerTouchListener;
import com.farid.moviesapi.model.MoviesList;
import com.farid.moviesapi.model.Result;
import com.farid.moviesapi.service.Utils;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    private RecyclerView mMovieList;
    private MoviesRvAdapter mMoviesRvAdapter;
    private MoviesList mMoviesList;
    private List<Result> mMovieResultList;
    private BaseApi mServiceApi;
    private int page = 1;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mMovieList = findViewById(R.id.movie_list);
        mServiceApi = BaseApiHandler.setupBaseApi().create(BaseApi.class);

        initData(page);

        mMovieList.addOnItemTouchListener(new RecyclerTouchListener(MainActivity.this, mMovieList, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                Intent mIntent = new Intent(MainActivity.this, MovieActivity.class);

                mIntent.putExtra("MovieResult", mMovieResultList.get(position));

                startActivity(mIntent);
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));
    }

    private void initData(int pageLocal) {
        if (Utils.isConnectionOn(this)) {
            Call<MoviesList> call = mServiceApi.
                    getMovieList(App.API_KEY, App.SORT_BY, App.INCLUDE_ADULT, pageLocal);
            call.enqueue(new Callback<MoviesList>() {
                @Override
                public void onResponse(Call<MoviesList> call, Response<MoviesList> response) {
                    if (response.isSuccessful()) {
                        try {
                            mMoviesList = response.body();
                            if (mMoviesList != null) {
                                mMovieResultList = mMoviesList.getResults();
                            }
                            mMovieList.setLayoutManager(new LinearLayoutManager(MainActivity.this));
                            mMoviesRvAdapter = new MoviesRvAdapter(MainActivity.this, mMovieResultList);
                            mMovieList.setAdapter(mMoviesRvAdapter);

                        } catch (Exception e) {
                            Log.e("Movie List Ex ", e.getMessage() + " ");
                        }

                    } else {
                        Log.e("Movie List Ex ", "else inside ");
                    }
                }

                @Override
                public void onFailure(Call<MoviesList> call, Throwable t) {

                }
            });


        } else {
            Log.e("Movie List Ex ", "else");
        }
    }
}
