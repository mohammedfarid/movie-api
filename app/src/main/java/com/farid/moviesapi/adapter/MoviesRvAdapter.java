package com.farid.moviesapi.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.farid.moviesapi.App;
import com.farid.moviesapi.R;
import com.farid.moviesapi.model.Result;
import com.squareup.picasso.Picasso;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class MoviesRvAdapter extends RecyclerView.Adapter<MoviesRvAdapter.ViewHolder> {

    private Context mContext;
    private List<Result> mMoviesListList;

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;

        public final CircleImageView mMovieIv;
        public final TextView mMovieTv;
        public final ImageView mMovieIvStar;
        public final TextView mMovieTvStar;

        public ViewHolder(View view) {
            super(view);
            mView = view;

            mMovieIv = view.findViewById(R.id.movie_iv);
            mMovieTv = view.findViewById(R.id.movie_tv);
            mMovieIvStar = view.findViewById(R.id.movie_iv_star);
            mMovieTvStar = view.findViewById(R.id.movie_tv_star);
        }
    }

    public MoviesRvAdapter(Context context, List<Result> moviesListList) {
        mContext = context;
        mMoviesListList = moviesListList;
        Log.d("movie", mMoviesListList.toString());
    }

    public Result getValueAt(int position) {
        return mMoviesListList.get(position);
    }

    @Override
    public int getItemCount() {
        return mMoviesListList.size();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.view_movie_list, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Result moviesList = getValueAt(position);
        if (moviesList != null) {
            if (moviesList.getOriginalTitle() != null && !moviesList.getOriginalTitle().equals(""))
                holder.mMovieTv.setText(moviesList.getOriginalTitle());
            if (moviesList.getBackdropPath() != null && !moviesList.getBackdropPath().equals(""))
                Picasso.get()
                        .load(App.API_IMAGE_URL + moviesList.getBackdropPath())
                        .into(holder.mMovieIv);

            holder.mMovieTvStar.setText(moviesList.getVoteAverage()+"");
        }
    }
}
