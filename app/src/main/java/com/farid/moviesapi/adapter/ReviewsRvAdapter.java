package com.farid.moviesapi.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.farid.moviesapi.R;
import com.farid.moviesapi.model.ResultReviews;
import com.squareup.picasso.Picasso;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class ReviewsRvAdapter extends RecyclerView.Adapter<ReviewsRvAdapter.ViewHolder> {
    private Context mContext;
    private List<ResultReviews> mResultReviewsList;

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;

        public final TextView mReviewTvPersonalName;
        public final TextView mReviewTvPersonalContent;

        public ViewHolder(View view) {
            super(view);
            mView = view;

            mReviewTvPersonalName = view.findViewById(R.id.personal_name_tv);
            mReviewTvPersonalContent = view.findViewById(R.id.personal_content_tv);

        }
    }

    public ReviewsRvAdapter(Context context, List<ResultReviews> resultReviewsList) {
        mContext = context;
        mResultReviewsList = resultReviewsList;
    }

    public ResultReviews getValueAt(int position) {
        return mResultReviewsList.get(position);
    }

    @Override
    public int getItemCount() {
        return mResultReviewsList.size();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.view_reviews_list, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        ResultReviews resultReviews = getValueAt(position);
        if (resultReviews != null) {
            if (resultReviews.getAuthor() != null && !resultReviews.getAuthor().equals(""))
                holder.mReviewTvPersonalName.setText(resultReviews.getAuthor());
            if (resultReviews.getContent() != null && !resultReviews.getContent().equals(""))
                holder.mReviewTvPersonalContent.setText(resultReviews.getContent());

        }
    }
}
