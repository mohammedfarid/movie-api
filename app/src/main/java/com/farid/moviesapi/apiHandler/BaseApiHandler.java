package com.farid.moviesapi.apiHandler;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.farid.moviesapi.App.API_BASE_URL;


/**
 * Created by Ahmed Moharm on 4/29/2018.
 */

public class BaseApiHandler {


    private static Retrofit retrofit = null;
    private static final int TIMEOUT_MIN = 3;

    public static Retrofit setupBaseApi() {
        if (retrofit == null) {
            HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
            OkHttpClient client = new OkHttpClient.Builder()
                    .connectTimeout(TIMEOUT_MIN, TimeUnit.MINUTES)
                    .readTimeout(TIMEOUT_MIN, TimeUnit.MINUTES)
                    .addInterceptor(interceptor).build();

            retrofit = new Retrofit.Builder()
                    .baseUrl(API_BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(client)
                    .build();
        }

        return retrofit;
    }

    public static Retrofit getRetrofit() {
        return retrofit;
    }

    public static void setRetrofit(Retrofit retrofit) {
        BaseApiHandler.retrofit = retrofit;
    }
}
