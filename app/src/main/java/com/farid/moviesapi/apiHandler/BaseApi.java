package com.farid.moviesapi.apiHandler;


import com.farid.moviesapi.model.AuthToken;
import com.farid.moviesapi.model.Genre;
import com.farid.moviesapi.model.MoviesList;
import com.farid.moviesapi.model.Reviews;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by Ahmed Moharm on 4/29/2018.
 */

public interface BaseApi {
    @GET("authentication/token/new")
    Call<AuthToken> getAuthToken(@Query("api_key") String ApiKey);

    @GET("genre/movie/list")
    Call<List<Genre>> getList(@Query("api_key") String ApiKey);


    @GET("movie/{movie_id}/reviews")
    Call<Reviews> getMoviesReviews(@Path("movie_id") String MovieId,
                                   @Query("api_key") String ApiKey);

    @GET("discover/movie")
    Call<MoviesList> getMovieList(@Query("api_key") String ApiKey,
                                  @Query("sort_by") String ListSortBy,
                                  @Query("include_adult") String IncludeAdult,
                                  @Query("page") Integer PageNumber);
}
