package com.farid.moviesapi;

import android.app.Application;
import android.content.Context;
import android.support.multidex.MultiDex;

public class App extends Application {
    public static final String API_BASE_URL = "https://api.themoviedb.org/3/";
    public static String API_KEY;
    public static final String INCLUDE_ADULT = "false";
    public static final String SORT_BY = "popularity.desc";
    public static final String API_IMAGE_URL = "http://image.tmdb.org/t/p/w300/";
    public static final String API_IMAGE_URL_V2 = "https://image.tmdb.org/t/p/w600_and_h900_bestv2/";

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
        API_KEY = base.getResources().getString(R.string.api);
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }

}
