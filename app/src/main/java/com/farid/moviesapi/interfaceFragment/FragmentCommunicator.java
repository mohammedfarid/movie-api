package com.farid.moviesapi.interfaceFragment;

import com.farid.moviesapi.model.Result;

public interface FragmentCommunicator {
    public void passDataToFragment(Result movieResult);
}
