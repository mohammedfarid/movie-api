package com.farid.moviesapi.interfaceFragment;

public interface ActivityCommunicator {
    public void passDataToActivity(String someValue);
}
