package com.farid.moviesapi.interfaceFragment;

import com.farid.moviesapi.model.ResultReviews;

import java.util.List;

public interface FragmentReviewCommunicator {
    public void passToFragment(List<ResultReviews> resultReviews);
}
